﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace WorkspaceControl
{
    public class ContextMenu
    {
        public string specialString;
        private List<ContextMenuButton> buttonContainer = new List<ContextMenuButton>();
        private readonly WorkspaceControl workspace;
        private Panel panel;
        private int maxWidth = 0;
        private readonly int fontSize = 0;
        public readonly Action<ContextMenuButton> ButtonUp_Callback = null; 
        public ContextMenu(Point location, WorkspaceControl wrkspace, int fntSize, Action<ContextMenuButton> btn_up_callback)
        {
            ButtonUp_Callback = btn_up_callback;

            workspace = wrkspace;
            fontSize = fntSize;

            panel = new Panel();
            panel.Visible = false;
            panel.Location = location;
            panel.Name = "contextPanel";
            panel.TabIndex = 0;
            workspace.Controls.Add(panel);
            panel.Name = "";
        }
        public ContextMenu(Point location, WorkspaceControl wrkspace, int fntSize, Control parentControl, Action<ContextMenuButton> btn_up_callback)
        {
            ButtonUp_Callback = btn_up_callback;

            workspace = wrkspace;
            fontSize = fntSize;

            panel = new Panel();
            panel.Visible = false;
            panel.Location = location;
            panel.Name = "contextPanel";
            panel.TabIndex = 0;
            parentControl.Controls.Add(panel);
        }
        public void Show()
        {
            foreach(ContextMenuButton btn in buttonContainer)
            {
                btn.UpdateWidth(maxWidth);
            }
            panel.Visible = true;
        }
        public void Hide()
        {
            panel.Visible = false;
        }
        public void Close()
        {
            panel.Visible = false;
            workspace.Controls.Remove(panel);
            workspace.SetContext(null);
        }
        public void Remove()
        {
            panel.Visible = false;
            if (panel.Parent != null)
                panel.Parent.Controls.Remove(panel);
        }
        public ContextMenuButton AddButton(string buttonName)
        {
            ContextMenuButton newButton = new ContextMenuButton(buttonName, fontSize, buttonContainer.Count, this);
            buttonContainer.Add(newButton);
            maxWidth = (newButton.GetWidth() > maxWidth) ? newButton.GetWidth() : maxWidth;
            panel.Size = new Size(maxWidth, newButton.GetHeight()*(buttonContainer.Count));
            return newButton;
        }
        public void InsertButton(int index, string buttonName)
        {
            ContextMenuButton newButton = new ContextMenuButton(buttonName, fontSize, index, this);
            buttonContainer.Insert(index, newButton);
            maxWidth = (newButton.GetWidth() > maxWidth) ? newButton.GetWidth() : maxWidth;
            panel.Size = new Size(maxWidth, newButton.GetHeight() * (buttonContainer.Count));
        }
        public void SortButtons()
        {
            int i = 0;
            foreach (ContextMenuButton btn in buttonContainer)
            {
                maxWidth = (btn.GetWidth() > maxWidth) ? btn.GetWidth() : maxWidth;
                panel.Size = new Size(maxWidth, btn.GetHeight() * (buttonContainer.Count));
                btn.SetIndex(i);
                btn.UpdateSize();
                i++;
            }
        }
        public Panel GetPanel()
        {
            return panel;
        }
        public WorkspaceControl GetWorkspace()
        {
            return workspace;
        }
        public Point GetPos()
        {
            return panel.Location;
        }
        public ContextMenuButton GetButton(string btnName)
        {
            return buttonContainer.Find(x => x.GetString() == btnName);
        }
        public void Toggle()
        {
            if (panel.Visible == true)
                Hide();
            else
                Show();
        }
    }
}
