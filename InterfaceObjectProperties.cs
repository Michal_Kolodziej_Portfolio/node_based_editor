﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WorkspaceControl
{
    public class InterfaceObjectProperties
    {   
        //PUBLIC EDITABLE PROPERTIES
        public class EditorProperties
        {
            public string objectTypeName;
            public string itemName;
            public Image backgroundImage;
            public Image hoverImage;
            public Image pressedImage;
            public Image additionalIconImage;
            public string labelText = "";
            public Color labelBaseColor = Color.Black;
            public Color labelHoverColor = Color.Black;
            public Color labelPressedColor = Color.Black;
            public int labelSize = 0;
            public FontFamily labelFont = null;
            public bool bExportable = true;
        }
        public EditorProperties EDITOR_PROPERTIES = new EditorProperties();
        public class HierarchyObject
        {
            public HierarchyObject(string newName, InterfaceObjectProperties newParentProperties, System.Windows.Forms.Panel pnl, HierarchyObject parentObj)
            {
                if (newParentProperties == null)
                    return;

                panelParent = pnl;

                int idx = 0;

                if (parentObj != null)
                {
                    parent = parentObj;
                    idx = parent.children.Count;
                    parent.children.Add(this);
                }
                else
                {
                    idx = newParentProperties.hierarchyObjects.Count;
                    newParentProperties.hierarchyObjects.Add(this);
                }

                name = newName;
                parentProperties = newParentProperties;

                label = new System.Windows.Forms.Label();
                label.Name = newName;
                label.TabIndex = idx;
                label.Text = name;
                Font fnt = new Font(SystemFonts.DefaultFont.FontFamily, 10f, FontStyle.Regular);
                label.Font = fnt;
                label.MouseEnter += Label_MouseEnter;
                label.MouseLeave += Label_MouseLeave;
                label.MouseClick += Label_MouseClick;

                if (panelParent != null)
                    panelParent.Controls.Add(label);
            }
            public HierarchyObject(string newName, InterfaceObjectProperties newParentProperties, System.Windows.Forms.Panel pnl)
            {
                if (newParentProperties == null)
                    return;

                panelParent = pnl;

                int idx = 0;

                if (newParentProperties.selectedHierarchyObj != null)
                {
                    parent = newParentProperties.selectedHierarchyObj;
                    idx = parent.children.Count;
                    parent.children.Add(this);
                }
                else
                {
                    idx = newParentProperties.hierarchyObjects.Count;
                    newParentProperties.hierarchyObjects.Add(this);
                }

                name = newName;
                parentProperties = newParentProperties;

                label = new System.Windows.Forms.Label();
                label.Name = newName;
                label.TabIndex = idx;
                label.Text = name;
                Font fnt = new Font(SystemFonts.DefaultFont.FontFamily, 10f, FontStyle.Regular);
                label.Font = fnt;
                label.MouseEnter += Label_MouseEnter;
                label.MouseLeave += Label_MouseLeave;
                label.MouseClick += Label_MouseClick;

                if (panelParent != null)
                    panelParent.Controls.Add(label);
            }

            public int GetEntireHierarchyChildrenCount()
            {
                int count = children.Count;
                foreach(HierarchyObject obj in children)
                {
                    count += obj.GetEntireHierarchyChildrenCount();
                }
                return count;
            }

            public int UpdateHierarchyPos()
            {
                int idx = 0;
                foreach (HierarchyObject child in children)
                {
                    if (child.parent != null)
                    {
                        int yPos = ((label.Font.Height + 10) * (idx + 1)) + child.parent.label.Location.Y;
                        int xPos = 20 + child.parent.label.Location.X;
                        child.label.Location = new Point(xPos, yPos);
                        idx += child.UpdateHierarchyPos();
                        idx++;
                    }
                }
                return idx;
            }

            public void DeleteHierarchy()
            {
                foreach(HierarchyObject child in children)
                {
                    child.DeleteHierarchy();
                    panelParent.Controls.Remove(child.label);
                }
                children.Clear();
            }

            public void Delete()
            {
                if (parent != null)
                    parent.children.Remove(this);
                panelParent.Controls.Remove(label);
                parentProperties.hierarchyObjects.Remove(this);
            }

            private void Label_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
            {
                if(parentProperties.selectedHierarchyObj == this)
                {
                    Select(false);
                    parentProperties.selectedHierarchyObj = null;
                    return;
                }

                if(parentProperties.selectedHierarchyObj != null)
                {
                    parentProperties.selectedHierarchyObj.Select(false);
                }

                parentProperties.selectedHierarchyObj = this;

                if(parentProperties.selectedHierarchyObj != null)
                {
                    parentProperties.selectedHierarchyObj.Select(true);
                }
            }

            private void Label_MouseLeave(object sender, EventArgs e)
            {
                if(!bSelected)
                    label.ForeColor = Color.Black;
            }

            private void Label_MouseEnter(object sender, EventArgs e)
            {
                label.ForeColor = Color.Green;
            }

            public void Select(bool sel)
            {
                bSelected = sel;

                if(sel)
                {
                    label.ForeColor = Color.Green;
                }
                else
                {
                    label.ForeColor = Color.Black;
                }
            }
            public bool IsSelected() { return bSelected; }
            public System.Windows.Forms.Label label = null;
            private System.Windows.Forms.Panel panelParent = null;
            public HierarchyObject parent = null;
            public InterfaceObjectProperties parentProperties = null;
            public List<HierarchyObject> children = new List<HierarchyObject>();
            public string name = "";
            private bool bSelected = false;
            public bool bTestModeExpanded = false;
        }
        //
        //PRIVATE NON-EDITABLE PROPERTIES
        public bool bIsDragging = false;
        public SizeF size;
        public SizeF baseSize;
        public PointF constPos = new PointF(0, 0);
        public PointF globalPos = new PointF(0, 0);
        public PointF posOnParent = new PointF(0, 0);
        public PointF lastCorrect_PosOnParent = new PointF(0, 0);
        public PointF lastPosOnParent = new PointF(0, 0);
        public bool bIsGroupSelected = false;
        public float scaleTemp = 0;
        public InterfaceObject parentObject = null;
        public List<InterfaceObject> children = new List<InterfaceObject>();
        public bool bVisible = true;
        public bool bVisibleByDefault = true;
        public bool bEditingInputText = false;
        public string inputTextCurrentString = "";
        public int inputTextMaxChars = 0;
        public int messageBoxMaxMessages = 0;
        public bool inputTextPassword = false;
        public List<string> dropDownItemList = new List<string>();
        public string dropDownListMainSelector = "";
        public bool bIsResizing = false;
        public bool bResizingX = false;
        public bool bResizingY = false;
        public float x_resize_start = 0f;
        public float y_resize_start = 0f;
        public bool bDropDownExpanded = false;
        public List<HierarchyObject> hierarchyObjects = new List<HierarchyObject>();
        public HierarchyObject selectedHierarchyObj = null;
        public string groupName = "";
        public bool bHide = false;
        public string dropClass = "";
        public void UpdateHierarchyPos()
        {
            int idx = 0;
            foreach (HierarchyObject hierarchyObj in hierarchyObjects)
            {
                if (hierarchyObj.parent == null)
                {
                    int yPos = (hierarchyObj.label.Font.Height + 10) * idx;
                    int xPos = 0;
                    hierarchyObj.label.Location = new Point(xPos, yPos);
                    hierarchyObj.UpdateHierarchyPos();
                    idx += 1 + hierarchyObj.GetEntireHierarchyChildrenCount();
                }
            }
        }
        public void DeleteCurrentHierarchy()
        {
            if(selectedHierarchyObj != null)
            {
                selectedHierarchyObj.DeleteHierarchy();
                selectedHierarchyObj.Delete();
                hierarchyObjects.Remove(selectedHierarchyObj);
                UpdateHierarchyPos();
            }
        }
        //
    }
}
