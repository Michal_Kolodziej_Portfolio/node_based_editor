﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WorkspaceControl
{ 
    public class WorkspaceDatabase
    {
        private readonly List<NodeBase> nodes = new List<NodeBase>();
        private readonly List<NodeBase> nodeSelected = new List<NodeBase>();
        private readonly List<InterfaceObject> interfaceObjects = new List<InterfaceObject>();
        private readonly List<InterfaceObject> interfaceObjectsSelected = new List<InterfaceObject>();
        private List<NodeTemplate> nodesRegistered = new List<NodeTemplate>();
        private List<InterfaceObjectTemplate> interfaceObjectsRegistered = new List<InterfaceObjectTemplate>();
        private List<NodeTemplate> hiddenRegistrations = new List<NodeTemplate>();
        private readonly WorkspaceControl workspace = null;
        public WorkspaceDatabase(WorkspaceControl wrks)
        {
            workspace = wrks;
        }
        public void AddNode(NodeBase node)
        {
            nodes.Add(node);
            if(node.editorProperties.bIsSingleUse)
            {
                HideRegisteredNode(node.editorProperties.EDITOR_PROPERTIES.nodeTypeName);
            }
        }
        public void AddInterfaceObject(InterfaceObject obj)
        {
            interfaceObjects.Add(obj);
        }
        public NodeBase GetNode(int index)
        {
            if (nodes.Count - 1 >= index)
                return nodes[index];
            return null;
        }
        public InterfaceObject GetInterfaceObject(int index)
        {
            if (interfaceObjects.Count - 1 >= index)
                return interfaceObjects[index];
            return null;
        }
        public int GetNodesCount()
        {
            return nodes.Count;
        }
        public int GetInterfaceObjectsCount()
        {
            return interfaceObjects.Count;
        }
        public void SelectNode(NodeBase node)
        {
            nodeSelected.Add(node);
        }
        public void SelectInterfaceObject(InterfaceObject obj)
        {
            interfaceObjectsSelected.Add(obj);
        }
        public void DeselectNode(NodeBase node)
        {
            nodeSelected.Remove(node);
        }
        public void DeselectInterfaceObject(InterfaceObject obj)
        {
            interfaceObjectsSelected.Remove(obj);
        }
        public bool IsSelected(NodeBase node)
        {
            return nodeSelected.Contains(node);
        }
        public bool IsSelected(InterfaceObject obj)
        {
            return interfaceObjectsSelected.Contains(obj);
        }
        public void DeselectAll()
        {
            foreach(NodeBase node in nodeSelected)
            {
                node.GroupSelectPure(false);
            }
            nodeSelected.Clear();

            foreach(InterfaceObject obj in interfaceObjectsSelected)
            {
                obj.GroupSelectPure(false);
            }
            interfaceObjectsSelected.Clear();
        }
        public bool HasSelection()
        {
            return (nodeSelected.Count > 0) || (interfaceObjectsSelected.Count > 0) ? true : false;
        }
        public void DeleteSelectedNodes()
        {
            foreach(NodeBase node in nodeSelected)
            {
                if (workspace.nodeHover == node)
                    workspace.nodeHover = null;

                node.Delete();
                nodes.Remove(node);
            }
            nodeSelected.Clear();
        }
        public void DeleteSelectedInterfaceObjects()
        {
            foreach(InterfaceObject obj in interfaceObjectsSelected)
            {
                if (workspace.objectHover == obj)
                    workspace.objectHover = null;

                interfaceObjects.Remove(obj);
            }
            interfaceObjectsSelected.Clear();
        }
        public void DeleteNode(NodeBase node)
        {
            node.Delete();
            nodes.Remove(node);
            if (node.editorProperties.bIsSingleUse)
            {
                UnhideRegisteredNode(node.editorProperties.EDITOR_PROPERTIES.nodeTypeName);
            }
        }
        public void DeleteInterfaceObject(InterfaceObject obj)
        {
            obj.Delete();
            interfaceObjects.Remove(obj);
        }
        public void FillContext(ContextMenu ctx)
        {
            if (ctx == null)
                return;

            foreach (InterfaceObjectTemplate objTemplate in interfaceObjectsRegistered)
            {
                ctx.AddButton(objTemplate.objectTypeName);
            }

            foreach (NodeTemplate nodeTemplate in nodesRegistered)
            {
                if (workspace.GetActiveNode() != null)
                {
                    if (workspace.nodeHover == null)
                    {
                        if (workspace.GetActiveNode().GetActivePlug() != null)
                        {
                            if (nodeTemplate.HasOppositePlugType(workspace.GetActiveNode().GetActivePlug().GetPlugType()))
                            {
                                if (nodeTemplate.bIsSingleUse)
                                {
                                    ctx.InsertButton(0, nodeTemplate.nodeTypeName);
                                }
                                else
                                    ctx.AddButton(nodeTemplate.nodeTypeName);
                            }
                        }

                    }
                }
                else
                {
                    if (nodeTemplate.bIsSingleUse)
                    {
                        ctx.InsertButton(0, nodeTemplate.nodeTypeName);
                    }
                    else
                        ctx.AddButton(nodeTemplate.nodeTypeName);
                }
            }
            ctx.SortButtons();
        }
        public void RegisterNode(NodeTemplate nodeTemplate)
        {
            nodesRegistered.Add(nodeTemplate);
        }
        public void RegisterInterfaceObject(InterfaceObjectTemplate objTemplate)
        {
            interfaceObjectsRegistered.Add(objTemplate);
        }
        public void RegisterAdvancedNode(NodeTemplate nodeTemplate)
        {
            nodesRegistered.Add(nodeTemplate);
        }
        public NodeTemplate GetRegisteredInfo(string nodeTypeName)
        {
            foreach(NodeTemplate nodeTemplate in nodesRegistered)
            {
                if (nodeTemplate.nodeTypeName == nodeTypeName)
                    return nodeTemplate;
            }
            return null;
        }
        public InterfaceObjectTemplate GetRegisteredInfo_Object(string objTypeName)
        {
            foreach (InterfaceObjectTemplate objTemplate in interfaceObjectsRegistered)
            {
                if (objTemplate.objectTypeName == objTypeName)
                    return objTemplate;
            }
            return null;
        }
        public List<NodeTemplate> GetRegisteredNodes()
        {
            return nodesRegistered;
        }
        public List<InterfaceObjectTemplate> GetRegisteredInterfaceObjects()
        {
            return interfaceObjectsRegistered;
        }
        public void HideRegisteredNode(string nodeTypeName)
        {
            NodeTemplate nodeTemplateByName = nodesRegistered.Find(x => x.nodeTypeName == nodeTypeName);
            hiddenRegistrations.Add(nodeTemplateByName);
            UnregisterNode(nodeTypeName);
        }
        public void UnhideRegisteredNode(string nodeTypeName)
        {
            NodeTemplate nodeTemplateByName = hiddenRegistrations.Find(x => x.nodeTypeName == nodeTypeName);
            nodesRegistered.Add(nodeTemplateByName);
            hiddenRegistrations.Remove(nodeTemplateByName);
        }
        public void UnregisterNode(string NodeTypeName)
        {
            nodesRegistered.Remove(nodesRegistered.Find(x => x.nodeTypeName == NodeTypeName));
        }
        public void UnregisterInterfaceObject(string objTypeName)
        {
            interfaceObjectsRegistered.Remove(interfaceObjectsRegistered.Find(x => x.objectTypeName == objTypeName));
        }
        public NodeTemplate GetNodeTemplateByName(string nodeTemplateType)
        {
            return nodesRegistered.Find(x => x.nodeTypeName == nodeTemplateType);
        }
        public InterfaceObjectTemplate GetInterfacteObjectTemplateByName(string objTemplateType)
        {
            return interfaceObjectsRegistered.Find(x => x.objectTypeName == objTemplateType);
        }
        public List<NodeBase> GetNodes()
        {
            return nodes;
        }
        public List<InterfaceObject> GetInterfaceObjects()
        {
            return interfaceObjects;
        }
        public NodeBase GetNode(string nodeType)
        {
            return nodes.Find(x => x.editorProperties.EDITOR_PROPERTIES.nodeTypeName == nodeType);
        }
        public InterfaceObject GetInterfaceObject(string objectTypeName)
        {
            return interfaceObjects.Find(x => x.editorProperties.EDITOR_PROPERTIES.objectTypeName == objectTypeName);
        }
        public InterfaceObject GetInterfaceObjectUnique(string objectItemName)
        {
            return interfaceObjects.Find(x => x.editorProperties.EDITOR_PROPERTIES.itemName == objectItemName);
        }
    }
}
