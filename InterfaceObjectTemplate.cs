﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WorkspaceControl
{
    public class InterfaceObjectTemplate
    {
        public readonly string objectTypeName;
        public readonly WorkspaceControl workspace;
        public readonly Size size;

        public InterfaceObjectTemplate(string objTypeName, WorkspaceControl workspaceParent, Size newSize)
        {
            objectTypeName = objTypeName;
            workspace = workspaceParent;
            size = newSize;
        }
    }
}
