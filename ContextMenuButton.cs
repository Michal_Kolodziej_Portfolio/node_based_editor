﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace WorkspaceControl
{
    public class ContextMenuButton
    {
        private readonly WorkspaceControl workspace;
        private Panel panel;
        private Label label;
        private readonly ContextMenu parentMenu;
        private int index = 0;
        private readonly int fontSize = 0;
        private readonly string name = "button";
        public ContextMenuButton(string buttonName, int fntSize, int idx, ContextMenu parMenu)
        {
            workspace = parMenu.GetWorkspace();
            parentMenu = parMenu;
            name = buttonName;
            fontSize = fntSize;
            index = idx;

            InitializePanel();
            InitializeLabel();
        }
        private void InitializePanel()
        {
            panel = new Panel();
            panel.BackColor = WorkspaceControl.furyGrayDark;
            panel.Name = "buttonPanel";
            panel.BorderStyle = BorderStyle.None;
            panel.TabIndex = 0;
            panel.MouseEnter += Panel_MouseEnter;
            panel.MouseLeave += Panel_MouseLeave;
            panel.MouseDown += Panel_MouseDown;
            panel.MouseUp += Panel_MouseUp;
            parentMenu.GetPanel().Controls.Add(panel);
        }

        private void InitializeLabel()
        {
            label = new Label();
            label.AutoSize = true;
            label.BackColor = Color.Transparent;
            label.Location = new Point(0, 0);
            label.Name = "buttonLabel";
            label.TextAlign = ContentAlignment.MiddleCenter;
            label.Font = new Font(label.Font.FontFamily, fontSize);
            label.Size = panel.Size;
            label.TabIndex = 1;
            label.Text = name;
            label.MouseEnter += Panel_MouseEnter;
            label.MouseLeave += Panel_MouseLeave;
            label.MouseUp += Panel_MouseUp;
            label.MouseDown += Panel_MouseDown;
            panel.Controls.Add(label);

            ResizePanel();
        }

        private void Panel_MouseLeave(object sender, EventArgs e)
        {
            panel.BackColor = WorkspaceControl.furyGrayDark;
        }

        private void Panel_MouseEnter(object sender, EventArgs e)
        {
            panel.BackColor = WorkspaceControl.furyGoldDark;
        }

        private void Panel_MouseDown(object sender, MouseEventArgs e)
        {
            panel.BackColor = WorkspaceControl.furyGoldOxygened;
        }

        private void Panel_MouseUp(object sender, MouseEventArgs e)
        {
            parentMenu.ButtonUp_Callback(this);
        }

        private void ResizePanel()
        { 
            int labelWidth = label.PreferredWidth;       
            int labelHeight = label.PreferredHeight;
            
            panel.Size = new Size(labelWidth + 30, labelHeight + 5);
            panel.Location = new Point(0, panel.Height * index);

            CentreLabel();
        }
        private void CentreLabel()
        {
            int labelWidth = label.PreferredWidth;
            int labelHeight = label.PreferredHeight;
            int panelWidth = panel.Width;
            int panelHeight = panel.Height;

            int widthDiff = panelWidth - labelWidth;
            int heightDiff = panelHeight - labelHeight;
            label.Location = new Point(widthDiff / 2, heightDiff / 2);
        }
        public void UpdateWidth(int width)
        {
            panel.Width = width;
            CentreLabel();
        }

        public int GetWidth()
        {
            return panel.Width;
        }
        public int GetHeight()
        {
            return panel.Height;
        }

        public string GetString()
        {
            return name;
        }
        public void SetIndex(int idx)
        {
            index = idx;
        }
        public void UpdateSize()
        {
            ResizePanel();
        }
        public string GetButtonName()
        {
            return name;
        }
        public void Deactivate()
        {
            panel.Enabled = false;
            label.Enabled = false;
        }
        public ContextMenu GetContext()
        {
            return parentMenu;
        }
    }
}
