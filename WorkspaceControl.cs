﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace WorkspaceControl
{
    public partial class WorkspaceControl : UserControl
    {
        public enum EZoomMovementMode
        {
            eZM_Cursor = 0, 
            eZM_Center
        }
        public enum EMovementMode
        {
            eMM_Strict = 0,
            eMM_Free
        }

        public ContextMenu additionalContext;
        //Settings
        public static bool bShowNodeInfo = false;
        public static float fZoomStepRatio = 0.1f;
        //
        public UserControl additionalUserControl = null;
        public float maxScale = 4.6f;
        public float scale = 1;
        private bool bSelectionRect = false;
        private bool bWorkspaceMovement = false;
        public RectangleF selectionRectangle = new RectangleF();
        private bool bMouseDown = false;
        public bool bIsGroupSelecting = false;
        public bool bIsGroupDeselecting = false;
        public bool bIsGroupDeleting = false;
        private Point mouseStartPos = new Point(0, 0);
        private Timer timer = new Timer();
        public NodeBase activeNode = null;
        public InterfaceObject activeObject = null;
        public NodePlug snapPlugA = null;
        public NodePlug snapPlugB = null;
        public NodePort snapPortA = null;
        public NodePort snapPortB = null;
        private Point mousepos = new Point(0, 0);
        public NodeBase nodeHover = null;
        public InterfaceObject objectHover = null;
        public SizeF maxSize = new SizeF(0, 0);
        public static readonly Color furyBlue = Color.FromArgb(255, 0, 146, 255);
        public static readonly Color furyGray = Color.FromArgb(255, 64, 64, 64);
        public static readonly Color furyGrayDark = Color.FromArgb(255, 45, 45, 45);
        public static readonly Color furyGrayXtreme = Color.FromArgb(255, 25, 25, 25);
        public static readonly Color furyGold = Color.FromArgb(255, 236, 214, 0);
        public static readonly Color furyGoldDark = Color.FromArgb(255, 219, 164, 9);
        public static readonly Color furyGoldOxygened = Color.FromArgb(255, 212, 171, 0);
        public static readonly Color furyLightLeatherBrown = Color.FromArgb(255, 137, 102, 82);
        public static readonly Color furyMilkyYellow = Color.FromArgb(255, 255, 232, 154);
        public static readonly Color furyDarkRed = Color.FromArgb(255, 128, 0, 0);
        public static readonly Color furyBloodyRed = Color.FromArgb(255, 115, 16, 16);
        public static readonly Color furySeaBlue = Color.FromArgb(255, 40, 121, 121);
        public static readonly Color furyDarkViolet = Color.FromArgb(255, 113, 66, 141);
        public static readonly Color furyCoolBlue = Color.FromArgb(255, 51, 110, 180);
        public static readonly Color furyBrown = Color.FromArgb(255, 131, 79, 48);
        public static readonly Pen furyPen = new Pen(Color.Gold, 5);
        private ContextMenu context = null;
        private NodeInfoContext nodeInfoCtx;
        public readonly WorkspaceDatabase database = null;
        private readonly NodeFactory nodeFactory = null;
        private readonly InterfaceObjectFactory interfaceObjectFactory = null;
        public string optionalFilePath;
        public Func<NodeTemplate, string> Node_Special_Callback;
        public Action<InterfaceObject, InterfaceObject.EMouseControlEvent> InterfaceObjectEventCallback = null;
        private Action<object, KeyEventArgs> globalKeyDown;
        public static Rectangle PRIMARY_SCREEN_RESOLUTION;
        public bool bNoContext = false;
        public bool bZoomable = true;
        public EZoomMovementMode iZoomMovementMode = EZoomMovementMode.eZM_Cursor;
        public EMovementMode iMovementMode = EMovementMode.eMM_Strict;
        public static bool bEditing = true;
        public WorkspaceControl friendWorkspace = null;
        public bool bInterfaceType = false;
        public InterfaceObject TestModeEditingInputText = null;
        public InterfaceObject resizedObject = null;
        public InterfaceObject resizedObject_Sealed = null;
        public InterfaceObject hoverObject_TestMode = null;
        public Form friendForm = null;
        public List<SwitchGroup> switchGroups = new List<SwitchGroup>();
        public WorkspaceControl(Func<NodeTemplate, string> f, Action<object, KeyEventArgs> globalKeyCallback)
        {
            PRIMARY_SCREEN_RESOLUTION = Screen.PrimaryScreen.Bounds;
            InitializeComponent();
            nodeInfoCtx = new NodeInfoContext(this);
            globalKeyDown = globalKeyCallback;

            Node_Special_Callback = f;

            database = new WorkspaceDatabase(this);

            nodeFactory = new NodeFactory(database, this);
            interfaceObjectFactory = new InterfaceObjectFactory(database, this);

            timer.Interval = 30;
            timer.Tick += TimerOnTick;
            timer.Start();
        }
        private void TimerOnTick(object sender, EventArgs eventArgs)
        {
           Invalidate();
            //if (this.Size.Width > maxSize.Width || this.Size.Height > maxSize.Height)
            //    maxSize = this.Size;
        }
        private void WorkspaceControl_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
            DrawConnections(e.Graphics);

            if (activeNode == null && activeObject == null && bSelectionRect && !database.HasSelection() && resizedObject_Sealed == null)
            {
                Point rectPos = mouseStartPos;
                Size rectSize = new Size(mousepos.X - mouseStartPos.X, mousepos.Y - mouseStartPos.Y);
                Point mouseDiff = new Point(mouseStartPos.X - mousepos.X, mouseStartPos.Y - mousepos.Y);
                //handling possibility of negative values
                //if WIDTH is negative
                if (rectSize.Width < 0)
                {
                    //in this case simple value flip won't work; we have to reverse the entire rectangle; position is mouse position now and size is mouse start pos - mouse pos
                    rectPos = new Point(mousepos.X, rectPos.Y);
                    rectSize = new Size(mouseDiff.X, rectSize.Height);
                }
                //same story with HEIGHT
                if (rectSize.Height < 0)
                {
                    rectPos = new Point(rectPos.X, mousepos.Y);
                    rectSize = new Size(rectSize.Width, mouseDiff.Y);
                }
                selectionRectangle = new RectangleF(rectPos, rectSize);

                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(50, WorkspaceControl.furyBlue)), selectionRectangle);
                e.Graphics.DrawRectangle(new Pen(Color.FromArgb(180, Color.Black), 3), Rectangle.Round(selectionRectangle));
            }
            else
                selectionRectangle = new RectangleF();
        }
        public void DrawConnections(Graphics g)
        {
            g.InterpolationMode = InterpolationMode.HighQualityBilinear;
            g.SmoothingMode = SmoothingMode.HighQuality;

            //draw current connection
            if (activeNode != null && activeNode.GetActivePlug() != null)
            {
                if (snapPlugB == null)
                {
                    if (context == null)
                        g.DrawLines(new Pen(activeNode.editorProperties.EDITOR_PROPERTIES.plugs_color, 3), FuryMath.GetCurveAlgorithmPoints(activeNode.GetActivePlugPoint(), mousepos));
                }
                else
                    g.DrawLines(new Pen(activeNode.editorProperties.EDITOR_PROPERTIES.plugs_color, 3), FuryMath.GetCurveAlgorithmPoints(activeNode.GetActivePlugPoint(), snapPlugB.GetPoint()));
            }
            else if(activeNode != null && activeNode.GetActivePort() != null)
            {
                if(snapPortB == null)
                {
                    if (context == null)
                        g.DrawLines(new Pen(activeNode.GetActivePort().variableColors[activeNode.GetActivePort().variableType], 3), FuryMath.GetCurveAlgorithmPoints(activeNode.GetActivePort().GetPoint(), mousepos));
                }
                else
                    g.DrawLines(new Pen(activeNode.GetActivePort().variableColors[activeNode.GetActivePort().variableType], 3), FuryMath.GetCurveAlgorithmPoints(activeNode.GetActivePort().GetPoint(), snapPortB.GetPoint()));
            }

            if (activeNode != null && snapPlugA != null && context != null)
                g.DrawLines(new Pen(snapPlugA.GetNode().editorProperties.EDITOR_PROPERTIES.plugs_color, 3), FuryMath.GetCurveAlgorithmPoints(snapPlugA.GetPoint(), context.GetPos()));

            else if (activeNode != null && snapPortA != null && context != null)
                g.DrawLines(new Pen(snapPortA.variableColors[snapPortA.variableType], 3), FuryMath.GetCurveAlgorithmPoints(snapPortA.GetPoint(), context.GetPos()));

            for (int i = 0; i < database.GetNodesCount(); i++)
            {
                if (database.GetNode(i) != null)
                {
                    database.GetNode(i).Draw(g);
                }
            }
            for (int i = 0; i < database.GetInterfaceObjectsCount(); i++)
            {
                if(database.GetInterfaceObject(i) != null)
                {
                    database.GetInterfaceObject(i).Draw(g);
                }
            }
            if (nodeHover != null && bShowNodeInfo)
            {
                if (nodeInfoCtx != null)
                {
                    if (!IsMouseDown())
                        nodeInfoCtx.Draw(g, nodeHover);
                }
            }
        }

        private void WorkspaceControl_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (!e.Shift)
            {
                bIsGroupSelecting = false;
            }
            if (!e.Control)
            {
                bIsGroupDeselecting = false;
            }
            if(e.KeyCode.ToString() == "Delete")
            {
                bIsGroupDeleting = false;
            }
        }
        public void PerformDelete()
        {
            if (!bIsGroupDeleting)
            {
                bIsGroupDeleting = true;
                if (activeNode == null)
                    database.DeleteSelectedNodes();
                else
                { 
                    database.DeleteNode(activeNode);
                    if (nodeHover == activeNode)
                        nodeHover = null;

                    activeNode = null;
                }
                if (activeObject == null)
                    database.DeleteSelectedInterfaceObjects();
                else
                {
                    database.DeleteInterfaceObject(activeObject);
                    if (objectHover == activeObject)
                        objectHover = null;

                    activeObject = null;
                }
            }
        }
        private void WorkspaceControl_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            globalKeyDown(sender, e);
            if(e.Shift)
            {
                bIsGroupSelecting = true;
                bIsGroupDeselecting = false;
            }
            if(e.Control)
            {
                bIsGroupSelecting = false;
                bIsGroupDeselecting = true;
            }
            if(e.KeyCode.ToString() == "Delete")
            {
                //if (!bIsGroupDeleting)
                //{
                //    bIsGroupDeleting = true;
                //    if (activeNode == null)
                //        database.DeleteSelectedNodes();
                //    else
                //        database.DeleteNode(activeNode);
                //}
                //activeNode = null;
            }
            if (e.KeyCode.ToString() == "Subtract")
            {
                if (scale < maxScale)
                {
                    scale += 1;
                    this.Scale(new SizeF(0.8f, 0.8f));
                    PreventOutOfBounds();
                }
            }
            if (e.KeyCode.ToString() == "Add")
            {
                if (scale > 1)
                {
                    scale -= 1;
                    this.Scale(new SizeF(1.25f, 1.25f));
                    PreventOutOfBounds();
                }
            }
        }

        private void WorkspaceControl_MouseClick(object sender, MouseEventArgs e)
        {
            if (bNoContext)
                return;

            if (context != null)
                context.Close();
            if (additionalContext != null)
                additionalContext.Close();
            //
            if (e.Button == MouseButtons.Right)
            {
                if (nodeHover == null && activeNode == null && (objectHover == null || objectHover.editorProperties.EDITOR_PROPERTIES.objectTypeName == "Panel"))
                    ShowContextMenu(e.Location);
                else if (nodeHover != null)
                {
                    if (nodeHover.editorProperties.plugHover != null)
                        nodeHover.editorProperties.plugHover.Disconnect();
                }
                else if (activeNode != null)
                {
                    if (activeNode.editorProperties.plugHover != null)
                        activeNode.editorProperties.plugHover.Disconnect();
                    else
                    {
                        activeNode = null;
                        ShowContextMenu(e.Location);
                    }
                }
                if(activeObject != null)
                {
                    activeObject.Select(false);
                    activeObject = null;
                    ShowContextMenu(e.Location);
                }
            }
        }

        private void WorkspaceControl_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                bMouseDown = false;
                bSelectionRect = false;
            }
            else if (e.Button == MouseButtons.Middle)
                bWorkspaceMovement = false;
        }

        private void WorkspaceControl_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                bMouseDown = true;
                bSelectionRect = true;
            }
            else if (e.Button == MouseButtons.Middle)
            {
                bWorkspaceMovement = true;
            }
            mouseStartPos = e.Location;
            if (e.Button == MouseButtons.Left)
            {
                if (!bIsGroupSelecting)
                {
                    if (activeNode != null)
                        activeNode.Select(false);
                    if (activeObject != null)
                        activeObject.Select(false);
                }
            }
        }

        private void WorkspaceControl_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            mousepos = e.Location;
            PositionWorkspace();
        }

        public void OnInterfaceObjectTestModeEvent(InterfaceObject obj, InterfaceObject.EMouseControlEvent eventMode)
        {
            if (InterfaceObjectEventCallback == null || obj == null)
                return;

            InterfaceObjectEventCallback(obj, eventMode);
        }

        private void WorkspaceControl_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!bZoomable)
                return;

            SplitterPanel parentContainer = (SplitterPanel)this.Parent;
            float x_middle = (parentContainer.Width / 2);
            float y_middle = (parentContainer.Height / 2);
            if (e.Delta < 0)
            {
                if (scale < maxScale)
                {
                    scale += fZoomStepRatio;
                    float x_axis = maxSize.Width / scale;
                    float y_axis = maxSize.Height / scale;
                    this.Size = new Size((int)x_axis, (int)y_axis);

                    if (iZoomMovementMode == EZoomMovementMode.eZM_Cursor)
                    {
                        float mouseMiddleDiff_x = ((mousepos.X + this.Location.X)) - x_middle;
                        float mouseMiddleDiff_y = ((mousepos.Y + this.Location.Y)) - y_middle;
                        Point newLocation = new Point((int)((this.Location.X) - mouseMiddleDiff_x), (int)((this.Location.Y) - mouseMiddleDiff_y));
                        newLocation = CorrectWithinBounds(newLocation);
                        this.Location = newLocation;
                    }
                    else if(iZoomMovementMode == EZoomMovementMode.eZM_Center)
                    {
                        Point newLocation = new Point((int)(x_middle - (this.Width / 2)), (int)(y_middle - (this.Height / 2)) - (int)(35 * scale));
                        this.Location = newLocation;
                    }
                }
            }
            else
            {
                //ZOOM IN
                if (scale > 1.03f)
                {
                    if (iZoomMovementMode == EZoomMovementMode.eZM_Cursor)
                    {
                        float mouseMiddleDiff_x = ((mousepos.X + this.Location.X)) - x_middle;
                        float mouseMiddleDiff_y = ((mousepos.Y + this.Location.Y)) - y_middle;
                        Point newLocation = new Point((int)((this.Location.X) - mouseMiddleDiff_x), (int)((this.Location.Y) - mouseMiddleDiff_y));
                        newLocation = CorrectWithinBounds(newLocation);
                        this.Location = newLocation;
                    }
                    else if (iZoomMovementMode == EZoomMovementMode.eZM_Center)
                    {
                        Point newLocation = new Point((int)(x_middle - (this.Width / 2)), (int)(y_middle - (this.Height / 2)) - (int)(35 * scale));
                        this.Location = newLocation;
                    }
                    scale -= fZoomStepRatio;
                    float x_axis = maxSize.Width / scale;
                    float y_axis = maxSize.Height / scale;
                    this.Size = new Size((int)x_axis, (int)y_axis);
                    if (scale == 1f)
                        this.Size = new Size((int)maxSize.Width, (int)maxSize.Height);
                }
                else
                {
                    this.Location = new Point(0, 0);
                }
            }
        }

        private void PositionWorkspace()
        {
            if (bWorkspaceMovement)
            {
                Point diff = new Point(mouseStartPos.X - mousepos.X, mouseStartPos.Y - mousepos.Y);
                Point newLoc = new Point(this.Location.X - diff.X, this.Location.Y - diff.Y);

                if (iMovementMode == EMovementMode.eMM_Strict)
                {
                    if (newLoc.X <= 0 && newLoc.X > (-(Size.Width - PRIMARY_SCREEN_RESOLUTION.Width)))
                        this.Location = new Point(newLoc.X, this.Location.Y);
                    if (newLoc.Y <= 0 && newLoc.Y > (-(Size.Height - PRIMARY_SCREEN_RESOLUTION.Height)))
                        this.Location = new Point(this.Location.X, newLoc.Y);

                    if (this.Location.X == 0 || this.Location.X == (-(Size.Width - PRIMARY_SCREEN_RESOLUTION.Width + 1)))
                        mouseStartPos.X = mousepos.X;
                    if (this.Location.Y == 0 || this.Location.Y == (-(Size.Height - PRIMARY_SCREEN_RESOLUTION.Height + 1)))
                        mouseStartPos.Y = mousepos.Y;
                }
                else if(iMovementMode == EMovementMode.eMM_Free)
                {
                    this.Location = newLoc;
                }
            }
        }

        private void PreventOutOfBounds()
        {
            //X
            int x_axis = this.Location.X;
            int y_axis = this.Location.Y;
            if (this.Location.X > 0)
                x_axis = 0;
            if (this.Location.X < (-(Size.Width - PRIMARY_SCREEN_RESOLUTION.Width + 1)))
                x_axis = (-(Size.Width - PRIMARY_SCREEN_RESOLUTION.Width + 1));
            //Y
            if (this.Location.Y > 0)
                y_axis = 0;
            if (this.Location.Y < (-(Size.Height - PRIMARY_SCREEN_RESOLUTION.Height + 1)))
                y_axis = (-(Size.Height - PRIMARY_SCREEN_RESOLUTION.Height + 1));

            this.Location = new Point(x_axis, y_axis);
        }
        private Point CorrectWithinBounds(Point posToCorrect)
        {
            //X
            int x_axis = posToCorrect.X;
            int y_axis = posToCorrect.Y;
            if (posToCorrect.X > 0)
                x_axis = 0;
            if (posToCorrect.X < (-(Size.Width - PRIMARY_SCREEN_RESOLUTION.Width + 1)))
                x_axis = (-(Size.Width - PRIMARY_SCREEN_RESOLUTION.Width + 1));
            //Y
            if (posToCorrect.Y > 0)
                y_axis = 0;
            if (posToCorrect.Y < (-(Size.Height - PRIMARY_SCREEN_RESOLUTION.Height + 1)))
                y_axis = (-(Size.Height - PRIMARY_SCREEN_RESOLUTION.Height + 1));

            posToCorrect = new Point(x_axis, y_axis);
            return posToCorrect;
        }

        public void ShowContextMenu(Point location)
        {
            if (context != null)
                context.Close();

            if (nodeHover != null)
                return; 

            context = new ContextMenu(location, this, 7, ContextMenuButton_Callback);

            database.FillContext(context);

            context.Show();
        }
        private void ContextMenuButton_Callback(ContextMenuButton btn)
        {
            Panel panel = context.GetPanel();
            ContextMenu parentMenu = context;

            panel.BackColor = WorkspaceControl.furyGoldDark;
            parentMenu.Close();

            bool buildNode = true;

            NodeTemplate nodeTemplate = database.GetRegisteredInfo(btn.GetButtonName());
            //NODE BUILDING
            if (nodeTemplate != null)
            {
                if (nodeTemplate.bSpecial)
                {
                    parentMenu.specialString = Node_Special_Callback(nodeTemplate);
                    buildNode = (parentMenu.specialString != null) || (parentMenu.specialString == "true");
                }

                if (buildNode)
                {
                    NodeBase node = GetNodeFactory().CreateNode(nodeTemplate);

                    if (parentMenu.specialString != null && parentMenu.specialString != "true")
                    {
                        int lastInvalidCharacter = parentMenu.specialString.LastIndexOf("\\");
                        string fileNameWithExtension = parentMenu.specialString.Remove(0, lastInvalidCharacter + 1);
                        string fileName = fileNameWithExtension.Remove(fileNameWithExtension.Length - 5, 5);

                        node.editorProperties.EDITOR_PROPERTIES.nodeDescription = fileName;
                        node.editorProperties.filePath = parentMenu.specialString;
                        parentMenu.specialString = null;
                    }
                    if (activeNode != null && snapPlugA != null)
                    {
                        if (snapPlugA.IsOutput())
                        {
                            PointF ctxPos = parentMenu.GetPos();
                            ctxPos = new PointF(ctxPos.X - node.GetInputPlugPointLocal().X, ctxPos.Y - node.GetInputPlugPointLocal().Y);
                            node.Move(ctxPos);
                            node.UpdateConstPos();
                            activeNode.AddChild(node);
                            snapPlugA = null;
                        }
                        else
                        {
                            PointF ctxPos = parentMenu.GetPos();
                            ctxPos = new PointF(ctxPos.X - node.GetOutputPlugPointLocal().X, ctxPos.Y - node.GetOutputPlugPointLocal().Y);
                            node.Move(ctxPos);
                            node.UpdateConstPos();
                            node.AddChild(activeNode);
                            snapPlugA = null;
                        }
                    }
                }
            }
            //Interface building
            else
            {
                InterfaceObjectTemplate objTemplate = database.GetRegisteredInfo_Object(btn.GetButtonName());
                if(objTemplate != null)
                {
                    InterfaceObject obj = GetInterfaceNodeFactory().CreateInterfaceObject(objTemplate, objectHover);
                    PointF ctxPos = parentMenu.GetPos();

                    obj.editorProperties.posOnParent = ctxPos;

                    if(objectHover != null)
                    {
                        objectHover.AddChild(obj);
                        //calculate position on parent
                        PointF curPos = objectHover.editorProperties.globalPos;
                        PointF correctedCtxPos = objectHover.GetClosestCorrectPointForChild(ctxPos, new SizeF(100, 100));
                        obj.editorProperties.posOnParent = new PointF(correctedCtxPos.X - curPos.X, correctedCtxPos.Y - curPos.Y);
                    }
                }
            }
        }      
        public NodeBase CreateSpecialNode(ContextMenuButton btn, string nodeTypeName)
        {
            Panel panel = btn.GetContext().GetPanel();
            ContextMenu parentMenu = btn.GetContext();

            panel.BackColor = WorkspaceControl.furyGoldDark;
            parentMenu.Close();

            NodeTemplate nodeTemplate = database.GetRegisteredInfo(nodeTypeName);
            NodeBase node = GetNodeFactory().CreateNode(nodeTemplate);
            if (activeNode != null && snapPlugA != null)
            {
                if (snapPlugA.IsOutput())
                {
                    PointF ctxPos = parentMenu.GetPos();
                    ctxPos = new PointF(ctxPos.X - node.GetInputPlugPointLocal().X, ctxPos.Y - node.GetInputPlugPointLocal().Y);
                    node.Move(ctxPos);
                    node.UpdateConstPos();
                    activeNode.AddChild(node);
                    snapPlugA = null;
                }
                else
                {
                    PointF ctxPos = parentMenu.GetPos();
                    ctxPos = new PointF(ctxPos.X - node.GetOutputPlugPointLocal().X, ctxPos.Y - node.GetOutputPlugPointLocal().Y);
                    node.Move(ctxPos);
                    node.UpdateConstPos();
                    node.AddChild(activeNode);
                    snapPlugA = null;
                }
            }
            return node;
        }

        public NodeBase CreateSpecialNode(string leafName, string nodeTypeName, Point mousePoint)
        {
            NodeTemplate nodeTemplate = database.GetRegisteredInfo(nodeTypeName);
            NodeBase node = GetNodeFactory().CreateNode(nodeTemplate);
            if (activeNode != null && snapPlugA != null)
            {
                if (snapPlugA.IsOutput())
                {
                    PointF ctxPos = mousePoint;
                    ctxPos = new PointF(ctxPos.X - node.GetInputPlugPointLocal().X, ctxPos.Y - node.GetInputPlugPointLocal().Y);
                    node.Move(ctxPos);
                    node.UpdateConstPos();
                    activeNode.AddChild(node);
                    snapPlugA = null;
                }
                else
                {
                    PointF ctxPos = mousePoint;
                    ctxPos = new PointF(ctxPos.X - node.GetOutputPlugPointLocal().X, ctxPos.Y - node.GetOutputPlugPointLocal().Y);
                    node.Move(ctxPos);
                    node.UpdateConstPos();
                    node.AddChild(activeNode);
                    snapPlugA = null;
                }
            }
            return node;
        }

        public void DeselectAllNodes()
        {
            if (activeNode != null)
                activeNode = null;

            database.DeselectAll();
        }

        public void CloseContextMenu()
        {
            if (context != null)
                context.Close();
        }

        public NodeFactory GetNodeFactory()
        {
            return nodeFactory;
        }

        public InterfaceObjectFactory GetInterfaceNodeFactory()
        {
            return interfaceObjectFactory;
        }

        public Point GetMousePos()
        {
            return mousepos;
        }

        public void SetMousePos(Point pos)
        {
            mousepos = pos;
        }
        public void SetContext(ContextMenu ctx)
        {
            context = ctx;
        }
        public bool IsMouseDown()
        {
            return bMouseDown;
        }
        public Point GetMouseStartPos()
        {
            return mouseStartPos;
        }
        public ContextMenu GetContextMenu()
        {
            return context;
        }
        public NodeBase GetActiveNode()
        {
            return activeNode;
        }
        public InterfaceObject GetActiveObject()
        {
            return activeObject;
        }
        public SwitchGroup GetSwitchGroupByName(string groupName)
        {
            foreach (SwitchGroup grp in switchGroups)
            {
                if (grp.GetGroupName() == groupName)
                {
                    return grp;
                }
            }
            return null;
        }
        public void AddSwitchToGroup(InterfaceObject obj)
        {
            if (obj == null)
                return;

            //firstly check if switch group exists; if it does, add item, if it doesn't - create it and add item
            SwitchGroup grp = GetSwitchGroupByName(obj.editorProperties.groupName);
            if(grp == null)
            {
                grp = new SwitchGroup(obj.editorProperties.groupName);
                switchGroups.Add(grp);
            }
            grp.AddGroupMember(obj);
        }
        public void ClearSwitchGroups()
        {
            foreach(SwitchGroup grp in switchGroups)
            {
                grp.Clear();
            }
            switchGroups.Clear();
        }
    }
}
