﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkspaceControl
{
    public class SwitchGroup
    {
        private readonly string groupName;
        private List<InterfaceObject> list = new List<InterfaceObject>();
        private InterfaceObject selectedSwitch = null;
        public SwitchGroup(string name)
        {
            groupName = name;
        }
        public void AddGroupMember(InterfaceObject obj)
        {
            list.Add(obj);
        }
        public void RemoveGroupMember(InterfaceObject obj)
        {
            list.Remove(obj);
        }
        public string GetGroupName() { return groupName; }
        public InterfaceObject GetActiveGroupMember()
        {
            return selectedSwitch;
        }
        public void Switch(InterfaceObject obj)
        {
            if (obj == null)
                return;

            if(obj == selectedSwitch)
            {
                obj.SwitchSelect(false);
                selectedSwitch = null;
            }
            else 
            {
                if (selectedSwitch != null)
                    selectedSwitch.SwitchSelect(false);

                selectedSwitch = obj;
                obj.SwitchSelect(true);
            }
        }
        public void Clear()
        {
            list.Clear();
        }
    }
}
