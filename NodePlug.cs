﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace WorkspaceControl
{
    public class NodePlug
    {
        public enum PlugPositioning
        {
            Plug_LeftTop = 0,
            Plug_MiddleTop,
            Plug_RightTop,
            Plug_LeftMid,
            Plug_Central,
            Plug_RightMid,
            Plug_LeftBottom,
            Plug_MiddleBottom,
            Plug_RightBottom
        }

        protected  WorkspaceControl workspace = null;
        protected Timer timer = new Timer();
        protected  NodeBase parentNode;
        protected int plugPositioning;
        protected Size plugSize = new Size(0, 0);
        protected Point connectionPoint = new Point(0, 0);
        public PointF connectionPointLocal = new Point(0, 0);
        protected int plugType;
        protected int index = -1;
        protected readonly PointF plugPosPercentage = new Point(0, 0);

        //public Color plug
        public NodePlug(NodeBase node, Size size, int positioning, int type)
        {
            plugPositioning = positioning;
            plugType = type;
            plugSize = size;
            parentNode = node;
            workspace = parentNode.GetWorkspace();

            var rect = new RectangleF(parentNode.CalculatePlugPosition(plugPositioning, plugSize, new PointF(0, 0)), plugSize);
            connectionPoint = new Point((int)rect.Location.X + (int)(rect.Width / 2), (int)rect.Location.Y + (int)(rect.Height / 2));
            connectionPointLocal = new PointF(connectionPoint.X - parentNode.GetCurrentPos().X, connectionPoint.Y - parentNode.GetCurrentPos().Y);
        }
        public NodePlug(NodeBase node, Size size, PointF perc, int type)
        {
            plugPosPercentage = perc;
            plugPositioning = -1;
            plugType = type;
            plugSize = size;
            parentNode = node;
            workspace = parentNode.GetWorkspace();

            var rect = new RectangleF(parentNode.CalculatePlugPosition(plugPositioning, plugSize, perc), plugSize);
            connectionPoint = new Point((int)rect.Location.X + (int)(rect.Width / 2), (int)rect.Location.Y + (int)(rect.Height / 2));
            connectionPointLocal = new PointF(connectionPoint.X - parentNode.GetCurrentPos().X, connectionPoint.Y - parentNode.GetCurrentPos().Y);
        }
        public NodePlug() { }

        virtual public void Draw(Graphics g)
        {
            plugSize = new Size((int)(parentNode.editorProperties.plugSize.Width / workspace.scale), (int)(parentNode.editorProperties.plugSize.Height / workspace.scale));
            //check if mouse is over this plug and highlight it eventually
            var rect = new RectangleF(parentNode.CalculatePlugPosition(plugPositioning, plugSize, plugPosPercentage), plugSize);
            connectionPoint = new Point((int)rect.Location.X + (int)(rect.Width / 2), (int)rect.Location.Y + (int)(rect.Height / 2));

            //if mouse is up, then we can check plug hover
            if (!workspace.IsMouseDown())
            {
                if (rect.Contains(workspace.GetMousePos()))
                {
                    //assign it only if parent node is selected
                    parentNode.editorProperties.plugHover = this;
                    g.FillRectangle(new SolidBrush(WorkspaceControl.furyGold), rect);
                }
                //if mouse is no longer over this plug, null it
                else if (parentNode.editorProperties.plugHover == this)
                {
                    parentNode.editorProperties.plugHover = null;
                }
                //if mouse is up then deactivate plug
                parentNode.SetActivePlug(null);
                //apply connection if mouse is up and this is snap plug
                if (workspace.snapPlugB == this)
                {
                    if (IsOutput())
                    {
                        if (!parentNode.editorProperties.bSingleChild || !parentNode.HasChildren())
                            parentNode.AddChild(workspace.activeNode);
                    }
                    else if (workspace.activeNode != null)
                        workspace.activeNode.AddChild(parentNode);

                    workspace.snapPlugB = null;
                    if (workspace.activeNode != null)
                        workspace.activeNode.SetActivePlug(null);
                }
            }
            //now if mouse is down and parent node is selected and this plug is hovered, then activate it
            else if (workspace.activeNode == parentNode && parentNode.editorProperties.plugHover == this)
            {
                if (IsOutput())
                {
                    if (!parentNode.editorProperties.bSingleChild || !parentNode.HasChildren())
                    {
                        parentNode.SetActivePlug(this);
                        workspace.snapPlugA = this;
                    }
                }
                else
                {
                    parentNode.SetActivePlug(this);
                    workspace.snapPlugA = this;
                }
            }
            //if plug is outside the node
            else if (parentNode.editorProperties.plugHover == this)
            {
                if (IsOutput())
                {
                    if (!parentNode.editorProperties.bSingleChild || !parentNode.HasChildren())
                    {
                        workspace.activeNode = parentNode;
                        parentNode.SetActivePlug(this);
                        workspace.snapPlugA = this;
                    }
                }
                else
                {
                    workspace.activeNode = parentNode;
                    parentNode.SetActivePlug(this);
                    workspace.snapPlugA = this;
                }
            }
            //default color
            if (parentNode.editorProperties.plugHover != this)
            {
                g.FillRectangle(new SolidBrush(parentNode.editorProperties.EDITOR_PROPERTIES.plugs_color), rect);
            }
            //if THIS is active plug then highlight it
            if (parentNode.GetActivePlug() == this)
            {
                g.FillRectangle(new SolidBrush(WorkspaceControl.furyGold), rect);
            }
            //if mouse is down and over this plug, check if it's linkable, but first check if connection is going on
            else if (workspace.activeNode != null && workspace.activeNode.GetActivePlug() != null)
            {
                if (workspace.IsMouseDown() && rect.Contains(workspace.GetMousePos()))
                {
                    if (CanLinkInto())
                    {
                        g.FillRectangle(new SolidBrush(WorkspaceControl.furyGold), rect);
                        workspace.snapPlugB = this;
                    }
                }
                else
                {
                    //if mouse is off the previously snapped plug, reset it
                    if (workspace.snapPlugB == this)
                    {
                        workspace.snapPlugB = null;
                    }
                }
            }
            g.DrawRectangle(new Pen(Color.Black, 2), Rectangle.Round(rect)); 
        }

        /// <summary>
        /// this method defines whether plug is linkable
        /// </summary>
        /// <returns></returns>
        public bool CanLinkInto()
        {
            bool bOK = false;
            //firstly check if active node exists
            if (workspace.activeNode != null)
            {
                //if currently active node's active plug is opposite type; 
                //if active node is not myself
                //if I am not parent of the active node
                bool specCond = true;
                if (IsOutput())
                    specCond = (!parentNode.editorProperties.bSingleChild || !parentNode.HasChildren());

                bOK = ((workspace.activeNode.GetActivePlug().GetPlugType() != GetPlugType()) && (workspace.activeNode != parentNode) && (workspace.activeNode.GetParent() != parentNode) && (parentNode.GetParent() != parentNode) && specCond );
            }


            return bOK;
        }
        public bool CanLinkFrom()
        {
            bool bOK = false;

            bool specCond = true;
            if (IsOutput())
                specCond = (!parentNode.editorProperties.bSingleChild || !parentNode.HasChildren());

            bOK = ((workspace.activeNode == parentNode && this == (parentNode.GetActivePlug()) || workspace.activeNode == null) && (specCond) );

            return bOK;
        }

        private void Panel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if(GetPlugType() == (int)NodeBase.PlugType.Plug_Input)
            {
                if(parentNode.GetParent() != null)
                {
                    parentNode.GetParent().RemoveChild(parentNode);
                }
            }
            else if(GetPlugType() == (int)NodeBase.PlugType.Plug_Output)
            {
                parentNode.RemoveAllChildren();
            }
        }
        public void Disconnect()
        {
            if(IsInput())
            {
                if(parentNode.GetParent() != null)
                {
                    parentNode.GetParent().RemoveChild(parentNode);
                    parentNode.SetParent(null);
                }
            }
            else if(IsOutput())
            {
                parentNode.RemoveAllChildren();
            }
        }

        public Point GetPoint()
        {
            return connectionPoint;
        }
        public int GetPlugType()
        {
            return plugType;
        }
        public bool IsOutput()
        {
            return (plugType == (int)NodeBase.PlugType.Plug_Output);
        }
        public bool IsInput()
        {
            return (plugType == (int)NodeBase.PlugType.Plug_Input);
        }
        public NodeBase GetNode()
        {
            return parentNode;
        }
    }
}
