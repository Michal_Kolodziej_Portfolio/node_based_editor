﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace WorkspaceControl
{
    public class NodeFactory
    {
        private readonly WorkspaceControl workspace;
        private readonly WorkspaceDatabase database = null;
        public NodeFactory(WorkspaceDatabase db, WorkspaceControl workspaceParent)
        {
            workspace = workspaceParent;
            database = db;
        }
        public NodeBase CreateNode(NodeTemplate nodeTemplate)
        {
            NodeBase node = new NodeBase(nodeTemplate);

            if (node != null)
                database.AddNode(node);
            return node;
        }
        public NodeBase CreateNode(NodeTemplate nodeTemplate, string description, Point location)
        {
            nodeTemplate.nodeDescription = description;
            NodeBase node = new NodeBase(nodeTemplate);

            database.AddNode(node);
            node.Move(location);
            return node;
        }
        public void RegisterNode(NodeTemplate nodeTemplate)
        {
            database.RegisterNode(nodeTemplate);
        }
        public void HideRegisteredNode(string nodeTypeName)
        {
            database.HideRegisteredNode(nodeTypeName);
        }
    }
}
